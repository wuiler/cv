<!--
https:// linkedin.com/in/ **Wuiler**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;twitter.com  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gitlab.com  -->
| **https://** | linkedin.com/in/ | twitter.com | gitlab.com | github.com | **Wuiler**  
| ------ | ------ | ------ | ------ | ------ |  


# Mi CV Contado - https://cv.wuiler.repl.run/
Hola! Soy $nombre, tengo $edad años y vivo en $ciudad. Casado y con 2 hijos.  
Hace más de 25 años que trabajo en entornos IT, enfocado principalmente en el desarrollo y me considero un ♨️ Artista del Software.  

Participo activamente en distinos proyectos de desarrollo de software como la Autogestión Académica de la UTNFRC (https://autogestion.frc.utn.edu.ar), del cual soy su creado junto con el grupo con el que trabajo. He desarrollado el Sistema de Trazabilidad Homepática (https://www.trameho.com.ar), un Sistema de Producción MRP completo (factoria.io), y muchos otros más.

Colaboro en diversos proyectos opensource y escribo sobre tema relacionados y pueden accerder a Github (https://github.com/wuiler/) 




Poseo amplios conocimientos en entornos web en donde he desarrollado varios proyectos tanto de backend como frontend, mencionando algunos de ellos la Autogestión Académica de la UTNFRC (https://autogestion.frc.utn.edu.ar), un Sistema de Trazabilidad Homepática (https://www.trameho.com.ar), un Sistema de Producción MRP completo (www.factoria.io),  entre otros.  
Durante un tiempo y lo sigo realizando en menor cuantía dedique mi tiempo a la seguridad informática, armando el site BlackSoul.com.ar para concientizar sobre la misma. Desarrolle experiencia a través de un curso brindado por Delloite sobre hacking ético.  


He programado desde mis comienzos comenzando con Basic, Pascal, Clipper, Fox y VisualFox; con este último, hace 24 años, realice mi primer aplicación con acceso a datos, una app de partes diarios de pozos petroleros.  

Ya en la Universidad UTNFRC, inicie con C y luego seguí mi vida con JAVA, participando y colaborando del primer Grupo de Programación Java, en donde realizamos la primer agenda universitaria con acceso a datos.  

Trabajado con lenguajes como Asp classic, Python, .Net, PHP, y Java siendo este mi lenguaje de cabecera y del cual hasta el dia de hoy sigo enamorado!  

He sido instructor y brinde capacitación a alumnos de los CTC (Centros Tecnológicos Comunitarios), en donde se brindaron herramientas y recursos para la creación de páginas web alla por año 2001.  

Manejo entornos Windows, Unix y Linux. Comence en entornos Unix y luego con windows server nt, 2000, 2003 y linux; use desde un redhat 5.2, slackware, debian's y ubuntu's desde sus comienzos y hasta la actualidad.  

He realizado implementaciones de servidores web desde Apache's, Nginx, Ligthing y WanServer.   

También implemente servidores de aplicaciones Java como Tomcat, Jetty, Wildfly y Jboss este ultimo es que uso en la actualidad en entornos clusterizados.  

Poseo amplios conocimientos en entornos web en donde he desarrollado varios proyectos tanto de backend como frontend, mencionando algunos de ellos la Autogestión Académica de la UTNFRC (https://autogestion.frc.utn.edu.ar), un Sistema de Trazabilidad Homepática (https://www.trameho.com.ar), un Sistema de Producción MRP completo (www.factoria.io),  entre otros.  
Durante un tiempo y lo sigo realizando en menor cuantía dedique mi tiempo a la seguridad informática, armando el site BlackSoul.com.ar para concientizar sobre la misma. Desarrolle experiencia a través de un curso brindado por Delloite sobre hacking ético.  

En la actualidad estoy aprendiendo DART, Flutter, Bulma, GabtsyJS y en menor cuantía NodeJS.

Gracias!
